# Case Study: 2D Pokemon

## Name
2D Pokemon Case Study Project

## Description
A small framework for a pokemon-like game. Featuring:
- 2D Topdown movement
- Battle System with weaknesses etc.
- Inventory Management
- Pokemon Management
