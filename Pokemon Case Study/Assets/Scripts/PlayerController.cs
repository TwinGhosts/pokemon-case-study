using Com.LuisPedroFonseca.ProCamera2D;
using Sirenix.OdinInspector;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Title("Properties")]
    [SerializeField] private float speed;

    [Title("Animation Clips")]
    [SerializeField] private AnimationClip animationIdleBottom;
    [SerializeField] private AnimationClip animationIdleLeft;
    [SerializeField] private AnimationClip animationIdleTop;
    [Space]
    [SerializeField] private AnimationClip animationWalkBottom;
    [SerializeField] private AnimationClip animationWalkLeft;
    [SerializeField] private AnimationClip animationWalkTop;

    [Title("References")]
    [SerializeField] private GameInputSettings gameInputSettings;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private Animator animator;
    [SerializeField] private Rigidbody2D body;

    private Vector2 movementInput;
    private float deadZone = 0.1f;
    private Vector2 previousInput;

    private void Awake()
    {
        BindControls();
        BindCamera();

        animator.Play(animationIdleBottom.name);
    }

    private void Update()
    {
        movementInput = gameInputSettings.Game.Move.ReadValue<Vector2>();
        Move();
    }

    private void Move()
    {
        if (movementInput.y > deadZone)
        {
            animator.Play(animationWalkTop?.name);
        }
        else if (movementInput.y < -deadZone)
        {
            animator.Play(animationWalkBottom?.name);
        }
        else if (movementInput.x > deadZone)
        {
            animator.Play(animationWalkLeft?.name);
            sprite.flipX = true;
        }
        else if (movementInput.x < -deadZone)
        {
            animator.Play(animationWalkLeft?.name);
            sprite.flipX = false;
        }

        if(movementInput.x < deadZone && 
            movementInput.x > -deadZone &&
            movementInput.y < deadZone &&
            movementInput.y > -deadZone)
        {
            if (previousInput.y > deadZone)
            {
                animator.Play(animationIdleTop?.name);
            }
            else if (previousInput.y < -deadZone)
            {
                animator.Play(animationIdleBottom?.name);
            }
            else if (previousInput.x > deadZone)
            {
                animator.Play(animationIdleLeft?.name);
                sprite.flipX = true;
            }
            else if (previousInput.x < -deadZone)
            {
                animator.Play(animationIdleLeft?.name);
                sprite.flipX = false;
            }
        }

        body.AddForce(movementInput * speed * Time.deltaTime);

        previousInput = movementInput;
    }

    private void BindControls()
    {
        gameInputSettings = new GameInputSettings();
        gameInputSettings.Game.Enable();            
    }

    private void BindCamera()
    {
        ProCamera2D.Instance?.AddCameraTarget(transform);
    }

    private void OnEnable()
    {
        gameInputSettings?.Game.Enable();
    }

    private void OnDisable()
    {
        gameInputSettings?.Game.Disable();
    }
}
